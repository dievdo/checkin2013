﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace CheckIn2013
{
    class Program
    {
        #region Init
        static string SourcePath = Properties.Settings.Default.SourcePath;
        static string SourceMask = Properties.Settings.Default.SourceMask;
        static string LogFile = Properties.Settings.Default.LogFile;
        static string FileInversionMO = Properties.Settings.Default.FileInversionMO;
        static string FileInversionOut = Properties.Settings.Default.FileInversionOut;
        static string FileInversionLoc = Properties.Settings.Default.FileInversionLoc;
        static string InversionFormat = Properties.Settings.Default.InversionFormat + '\n';
        //string static string FileBankier = Properties.Settings.Default.FileBankier;

        const int CutLength = 20;
        const string OurBIC = "044030702";
        const string OurKS = "30101810600000000702";
        const string OurINN = "7831001422";
        const string OurName = "АО \"Сити Инвест Банк\"";
        
        static Encoding cp866 = Encoding.GetEncoding(866);

        public struct PayDoc
        {
            public string DocNo;
            public string DocDate;
            public string Sum;
            public string Queue;
            public string Details;
            public string INN;
            public string KPP;
            public string LS;
            public string BIC;
            public string KS;
            public string Name2;
            public string INN2;
            public string KPP2;
            public string LS2;
            public string BIC2;
            public string KS2;
            public string SS;
            public string NAL1;
            public string NAL2;
            public string NAL3;
            public string NAL4;
            public string NAL5;
            public string NAL6;
            public string NAL7;
            public string OpKind;
            public string PayCode; //since 31.03.2014
        }
        #endregion

        static void Main(string[] args)
        {
            int cnt = 0;
            foreach (string filename in Directory.GetFiles(SourcePath, SourceMask))
            {
                DataTable DBFTable = new DataTable();

                Console.WriteLine("Reading {0}", filename);
                ReadDBF(filename, DBFTable);///////////////////////////
                foreach (DataRow Rec in DBFTable.Rows)
                {
                    #region Test
                    //foreach (object Value in Rec.ItemArray)
                    //{
                    //    Console.WriteLine(Value.ToString());
                    //}
                    //Console.ReadLine();

                    //for (int col = 0; col < GTable.Columns.Count; col++)
                    //{
                    //    string s = GTable.Columns[col].Caption;
                    //    Console.WriteLine("{0} = {1}", s, Rec.ItemArray[col].ToString());
                    //}
                    //Console.WriteLine("!!! Payer = {0}", Rec["PAYER"].ToString());
                    //Console.ReadLine();
                    #endregion

                    #region ReadDoc
                    PayDoc doc = new PayDoc { };
                    doc.DocNo = Rec["NUMBER"].ToString();
                    doc.DocDate = Rec["DATE"].ToString();
                    doc.Sum = Rec["SUM"].ToString();
                    doc.Queue = Rec["PAY_QUEUE"].ToString();
                    doc.Details = StripSpaces(Rec["PAYMENT_AI"].ToString());

                    doc.INN = Rec["PAYER_INN"].ToString();
                    doc.KPP = Rec["PAYER_KPP"].ToString();
                    doc.LS = Rec["PAYER_ACC"].ToString();
                    doc.BIC = OurBIC;
                    doc.KS = OurKS;

                    doc.Name2 = StripSpaces(Rec["RECIP"].ToString());
                    doc.INN2 = Rec["RECIP_INN"].ToString();
                    doc.KPP2 = Rec["RECIP_KPP"].ToString();
                    doc.LS2 = Rec["RECIP_ACC"].ToString();
                    doc.BIC2 = Rec["RECIP_BIC"].ToString();
                    doc.KS2 = Rec["RECIP_KS"].ToString();

                    if (DBFTable.Columns.Contains("TAX_STATUS"))
                    {
                        doc.SS = Rec["TAX_STATUS"].ToString();
                        doc.NAL1 = Rec["KBK"].ToString();
                        doc.NAL2 = Rec["OKATO"].ToString();
                        doc.NAL3 = Rec["TAX_REASON"].ToString();
                        doc.NAL4 = Rec["TAX_PERIOD"].ToString();
                        doc.NAL5 = Rec["TAX_DOC_N"].ToString();
                        doc.NAL6 = Rec["TAX_DATE"].ToString();
                        //doc.NAL7 = Rec["TAX_TYPE"].ToString();
                        if (DBFTable.Columns.Contains("TAX_TYPE")) //since 30.03.2015
                            doc.NAL7 = Rec["TAX_TYPE"].ToString();
                        else
                            doc.NAL7 = "";

                        if (DBFTable.Columns.Contains("PAYCODE")) //since 31.03.2014
                            doc.PayCode = Rec["PAYCODE"].ToString();
                        else
                            doc.PayCode = "";
                    }
                    else
                    {
                        doc.SS = "";
                        doc.NAL1 = "";
                        doc.NAL2 = "";
                        doc.NAL3 = "";
                        doc.NAL4 = "";
                        doc.NAL5 = "";
                        doc.NAL6 = "";
                        doc.NAL7 = "";

                        doc.PayCode = ""; //since 31.03.2014
                    }

                    doc.OpKind = Rec["OP_KIND"].ToString();

                    bool local = doc.BIC2.Equals(doc.BIC);
                    #endregion

                    Console.WriteLine("{0,4} N{1,-3} {2,18} {3}", ++cnt, doc.DocNo, doc.Sum, doc.Name2);
                    #region Rules
                    DateTimeFormatInfo dfi = new CultureInfo("ru-RU", false).DateTimeFormat;
                    NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

                    string part = "Номер документа";
                    {
                        uint test;
                        while (!UInt32.TryParse(doc.DocNo, NumberStyles.None, nfi, out test))
                            Problem(doc, part, ref doc.DocNo, "не число");
                    }

                    part = "Дата док.";
                    {
                        DateTime test;
                        while (!DateTime.TryParseExact(doc.DocDate, "dd.MM.yyyy", dfi, DateTimeStyles.None, out test))
                            Problem(doc, part, ref doc.DocDate, "не дата");
                        if (doc.OpKind.Equals("01")) //Платежное поручение и только
                        {
                            while (DateTime.Compare(test, test.AddDays(-10)) < 0)
                                Problem(doc, part, ref doc.DocDate, "старее 10 дней");
                            while (DateTime.Compare(test, test.AddDays(5)) > 0)
                                Problem(doc, part, ref doc.DocDate, "позднее 5 дней");
                        }
                    }

                    part = "Сумма плат.";
                    {
                        decimal test;
                        while (!Decimal.TryParse(doc.Sum, NumberStyles.AllowDecimalPoint, nfi, out test))
                            Problem(doc, part, ref doc.Sum, "не сумма");
                    }

                    part = "Очер. плат.";
                    {
                        uint test;
                        while (!UInt32.TryParse(doc.Queue, out test))
                            Problem(doc, part, ref doc.Queue, "не число");
                        ProbEx(doc, part, ref doc.Queue, Properties.Settings.Default.REGEXP_QUE);
                    }

                    part = "ИНН плат.";
                    VerifyINN(doc, part, ref doc.INN, doc.LS);

                    part = "КПП плат.";
                    ProbEx(doc, part, ref doc.KPP, Properties.Settings.Default.REGEXP_KPP);

                    part = "Счет плат.";
                    VerifyLS(doc, part, ref doc.LS, OurBIC);

                    part = "Получатель";
                    VerifyText(doc, part, ref doc.Name2, 3, 160);

                    part = "ИНН получ.";
                    VerifyINN(doc, part, ref doc.INN2, doc.LS2);

                    part = "КПП получ.";
                    ProbEx(doc, part, ref doc.KPP2, Properties.Settings.Default.REGEXP_KPP);

                    part = "Счет получ.";
                    VerifyLS(doc, part, ref doc.LS2, doc.BIC2);

                    part = "Код плат."; //since 31.03.2014
                    VerifyText(doc, part, ref doc.PayCode, 0, 25);
                    if (doc.PayCode.Length > 0)
                        Problem(doc, part, ref doc.PayCode, "должно быть до 25 знаков");

                    #endregion

                    #region Bankier
                    //Банкир
                    //string[] doc1 = new string[103];

                    //doc1[1] = DMY2YMD(doc.DocDate);
                    //doc1[2] = doc.DocNo;
                    //doc1[3] = doc.OpKind;
                    //doc1[4] = "A";
                    //doc1[9] = local ? "801" : "802";
                    //doc1[10] = "4";
                    //doc1[11] = "810";
                    //doc1[12] = doc.Sum;
                    //doc1[13] = doc.Sum;
                    //doc1[15] = doc.Queue;
                    //doc1[19] = doc.LS;
                    //doc1[23] = doc.BIC;
                    //doc1[25] = doc.LS2;
                    //doc1[27] = doc.KS2;
                    //doc1[29] = doc.BIC2;
                    //doc1[31] = doc.Details;
                    //doc1[35] = doc.Name2;
                    //doc1[48] = "e";
                    //doc1[49] = doc.LS;
                    //doc1[51] = local ? "" : "SM1";
                    //doc1[53] = doc.OpKind.Equals("01") ? "26" : "5";
                    //doc1[54] = "e";
                    //doc1[55] = local ? doc.LS2 : doc.KS;
                    //doc1[73] = "243";
                    //doc1[75] = doc.INN;
                    //doc1[76] = doc.INN2;
                    //doc1[92] = doc.KPP;
                    //doc1[93] = doc.KPP2;
                    //doc1[94] = doc.SS;
                    //doc1[95] = doc.NAL1;
                    //doc1[96] = doc.NAL2;
                    //doc1[97] = doc.NAL3;
                    //doc1[98] = doc.NAL4;
                    //doc1[99] = doc.NAL5;
                    //doc1[100] = doc.NAL6;
                    //doc1[101] = doc.NAL7;
                    //doc1[102] = DateTime.Today.ToString("yyyyMMdd");

                    //string bankier = String.Join("^", doc1, 1, 102);
                    //File.AppendAllText(FileBankier, bankier + '\n', cp866);
                    #endregion

                    #region Inversia
                    //Инверсия
                    StringBuilder doc2 = new StringBuilder("# Doc Begin\n");
                    if (local) //mo
                    //{
                    //    doc2.AppendFormat(InversionFormat, "Address", "0");
                    //    doc2.AppendFormat(InversionFormat, "Deb_Acc", doc.LS);
                    //    doc2.AppendFormat(InversionFormat, "Deb_Cur", "RUR");
                    //    doc2.AppendFormat(InversionFormat, "Deb_Sum", doc.Sum);
                    //    doc2.AppendFormat(InversionFormat, "Cre_Acc", doc.LS2);
                    //    doc2.AppendFormat(InversionFormat, "Cre_Cur", "RUR");
                    //    doc2.AppendFormat(InversionFormat, "Cre_Sum", doc.Sum);
                    //    doc2.AppendFormat(InversionFormat, "BO1", "1");
                    //    doc2.AppendFormat(InversionFormat, "BO2", "");
                    //    doc2.AppendFormat(InversionFormat, "Date_Reg", doc.DocDate);
                    //    doc2.AppendFormat(InversionFormat, "Date_Doc", doc.DocDate);
                    //    doc2.AppendFormat(InversionFormat, "Date_Val", doc.DocDate);
                    //    doc2.AppendFormat(InversionFormat, "Date_Trn", doc.DocDate);
                    //    doc2.AppendFormat(InversionFormat, "Doc_Num", doc.DocNo);
                    //    doc2.AppendFormat(InversionFormat, "Batch_Num", "26");
                    //    doc2.AppendFormat(InversionFormat, "Client_Name", "" /*OurName*/);
                    //    doc2.AppendFormat(InversionFormat, "Client_INN", doc.INN);
                    //    doc2.AppendFormat(InversionFormat, "Corr_Name", doc.Name2);
                    //    doc2.AppendFormat(InversionFormat, "Corr_INN", doc.INN2);
                    //    doc2.AppendFormat(InversionFormat, "Purpose", doc.Details);
                    //    doc2.AppendFormat(InversionFormat, "User", "" /*"XXI"*/);
                    //    doc2.AppendFormat(InversionFormat, "Cashier", "");
                    //    doc2.AppendFormat(InversionFormat, "CoCode", "");
                    //    doc2.AppendFormat(InversionFormat, "ZakOb", "0");

                    //    File.AppendAllText(FileInversionMO, doc2.Append("# Doc End\n").ToString(), cp866);
                    // }
                     {
                        //StringBuilder doc2 = new StringBuilder("# Doc Begin\n");
                        //doc2.Remove(0, doc2.Length);
                        //doc2.Append("# Doc Begin\n");

                         string batch = doc.OpKind.Equals("01") ? "26" : "5"; //Batch_Num

                        doc2.AppendFormat(InversionFormat, "Address", "0");
                        doc2.AppendFormat(InversionFormat, "BO1", "2");
                        doc2.AppendFormat(InversionFormat, "BO2", "");
                        doc2.AppendFormat(InversionFormat, "Date_Reg", doc.DocDate);
                        doc2.AppendFormat(InversionFormat, "Date_Doc", doc.DocDate);
                        doc2.AppendFormat(InversionFormat, "Date_Val", doc.DocDate);
                        doc2.AppendFormat(InversionFormat, "Doc_Num", doc.DocNo);
                        doc2.AppendFormat(InversionFormat, "Batch_Num", batch);
                        doc2.AppendFormat(InversionFormat, "Priority", doc.Queue);
                        doc2.AppendFormat(InversionFormat, "Purpose", doc.Details);
                        doc2.AppendFormat(InversionFormat, "Summa", doc.Sum);
                        doc2.AppendFormat(InversionFormat, "Currency", "RUR");
                        doc2.AppendFormat(InversionFormat, "Date_Shadow", doc.DocDate);
                        doc2.AppendFormat(InversionFormat, "DWay_Type", "E");
                        doc2.AppendFormat(InversionFormat, "VO", doc.OpKind);
                        doc2.AppendFormat(InversionFormat, "Cus_SB", "");
                        doc2.AppendFormat(InversionFormat, "Payer_Acc", doc.LS);
                        doc2.AppendFormat(InversionFormat, "Recipient_Acc", doc.LS2);
                        doc2.AppendFormat(InversionFormat, "Client_Name", "");
                        doc2.AppendFormat(InversionFormat, "Client_INN", doc.INN);
                        doc2.AppendFormat(InversionFormat, "Client_CorAcc", "" /*doc.KS*/);
                        doc2.AppendFormat(InversionFormat, "Corr_RBIC", doc.BIC2);
                        doc2.AppendFormat(InversionFormat, "Corr_CorAcc", doc.KS2);
                        doc2.AppendFormat(InversionFormat, "Corr_Bank_Name", "");
                        doc2.AppendFormat(InversionFormat, "Corr_Name", doc.Name2);
                        doc2.AppendFormat(InversionFormat, "Corr_INN", doc.INN2);
                        doc2.AppendFormat(InversionFormat, "User", "" /*"XXI"*/);
                        doc2.AppendFormat(InversionFormat, "KPP", doc.KPP);
                        doc2.AppendFormat(InversionFormat, "KPPPOL", doc.KPP2);
                        doc2.AppendFormat(InversionFormat, "STATUSSOSTAVIT", doc.SS);
                        doc2.AppendFormat(InversionFormat, "KBK_F", doc.NAL1);
                        doc2.AppendFormat(InversionFormat, "OKATO", doc.NAL2);
                        doc2.AppendFormat(InversionFormat, "POKOSNPLAT", doc.NAL3);
                        doc2.AppendFormat(InversionFormat, "POKNALPERIOD", doc.NAL4);
                        doc2.AppendFormat(InversionFormat, "POKNUMDOC", doc.NAL5);
                        doc2.AppendFormat(InversionFormat, "POKDATEDOC", doc.NAL6);
                        doc2.AppendFormat(InversionFormat, "POKTYPEPLAT", doc.NAL7);
                        doc2.AppendFormat(InversionFormat, "Doc_Index", doc.PayCode);

                        File.AppendAllText(FileInversionLoc, doc2.Append("# Doc End\n").ToString(), cp866);
                    }
                    else //out
                    {
                        //string batch = doc.OpKind.Equals("01") ? "26" : "5"; //Batch_Num
                        //default for "01":
                        string batch = "26";
                        string bo1 = "4";

                        switch (doc.OpKind)
                        {
                            case "02":
                                batch = "5";
                                bo1 = "15";
                                break;
                            case "06":
                                batch = "5";
                                bo1 = "23";
                                break;
                            default:
                                break;
                        }

                        doc2.AppendFormat(InversionFormat, "Address", "0");
                        doc2.AppendFormat(InversionFormat, "BO1", bo1);
                        doc2.AppendFormat(InversionFormat, "BO2", "");
                        doc2.AppendFormat(InversionFormat, "Date_Reg", doc.DocDate);
                        doc2.AppendFormat(InversionFormat, "Date_Doc", doc.DocDate);
                        doc2.AppendFormat(InversionFormat, "Date_Val", doc.DocDate);
                        doc2.AppendFormat(InversionFormat, "Doc_Num", doc.DocNo);
                        doc2.AppendFormat(InversionFormat, "Batch_Num", batch);
                        doc2.AppendFormat(InversionFormat, "Priority", doc.Queue);
                        doc2.AppendFormat(InversionFormat, "Purpose", doc.Details);
                        doc2.AppendFormat(InversionFormat, "Summa", doc.Sum);
                        doc2.AppendFormat(InversionFormat, "Currency", "RUR");
                        doc2.AppendFormat(InversionFormat, "Date_Shadow", doc.DocDate);
                        doc2.AppendFormat(InversionFormat, "DWay_Type", "E");
                        doc2.AppendFormat(InversionFormat, "VO", doc.OpKind);
                        doc2.AppendFormat(InversionFormat, "Cus_SB", "");
                        doc2.AppendFormat(InversionFormat, "Payer_Acc", doc.LS);
                        doc2.AppendFormat(InversionFormat, "Recipient_Acc", doc.LS2);
                        doc2.AppendFormat(InversionFormat, "Client_Name", "");
                        doc2.AppendFormat(InversionFormat, "Client_INN", doc.INN);
                        doc2.AppendFormat(InversionFormat, "Client_CorAcc", "" /*doc.KS*/);
                        doc2.AppendFormat(InversionFormat, "Corr_RBIC", doc.BIC2);
                        doc2.AppendFormat(InversionFormat, "Corr_CorAcc", doc.KS2);
                        doc2.AppendFormat(InversionFormat, "Corr_Bank_Name", "");
                        doc2.AppendFormat(InversionFormat, "Corr_Name", doc.Name2);
                        doc2.AppendFormat(InversionFormat, "Corr_INN", doc.INN2);
                        doc2.AppendFormat(InversionFormat, "User", "" /*"XXI"*/);
                        doc2.AppendFormat(InversionFormat, "KPP", doc.KPP);
                        doc2.AppendFormat(InversionFormat, "KPPPOL", doc.KPP2);
                        doc2.AppendFormat(InversionFormat, "STATUSSOSTAVIT", doc.SS);
                        doc2.AppendFormat(InversionFormat, "KBK_F", doc.NAL1);
                        doc2.AppendFormat(InversionFormat, "OKATO", doc.NAL2);
                        doc2.AppendFormat(InversionFormat, "POKOSNPLAT", doc.NAL3);
                        doc2.AppendFormat(InversionFormat, "POKNALPERIOD", doc.NAL4);
                        doc2.AppendFormat(InversionFormat, "POKNUMDOC", doc.NAL5);
                        doc2.AppendFormat(InversionFormat, "POKDATEDOC", doc.NAL6);
                        doc2.AppendFormat(InversionFormat, "POKTYPEPLAT", doc.NAL7);
                        doc2.AppendFormat(InversionFormat, "Doc_Index", doc.PayCode);

                        File.AppendAllText(FileInversionOut, doc2.Append("# Doc End\n").ToString(), cp866);
                    }

                    #endregion
                }
                string bak = Path.Combine(SourcePath, string.Format(@"BAK\{0:yyyy}\{0:MM}\{0:dd}\{0:HHmm}{1}", DateTime.Now, Path.GetFileName(filename)));
                string path=Path.GetDirectoryName(bak);
                if(!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                File.Move(filename, bak);
                if (File.Exists(bak))
                    File.AppendAllText(LogFile, string.Format("{0} {1} ok\n", filename, DBFTable.Rows.Count), cp866);
                else
                    File.AppendAllText(LogFile, string.Format("{0} не создать"), cp866);
                Console.WriteLine();
            }
            Console.WriteLine("Press Enter");
            Console.ReadLine();
        }

        private static void ReadDBF(string filename, DataTable table)
        {
            // http://nansoft.ru/blog/csharp/30.html

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                byte[] buffer = new byte[4]; // Кол-во записей: 4 байтa, начиная с 5-го
                fs.Position = 4;
                fs.Read(buffer, 0, buffer.Length);
                int RowsCount = buffer[0] + (buffer[1] * 0x100) + (buffer[2] * 0x10000) + (buffer[3] * 0x1000000);

                buffer = new byte[2]; // Кол-во полей: 2 байтa, начиная с 9-го
                fs.Position = 8;
                fs.Read(buffer, 0, buffer.Length);
                int FieldCount = (((buffer[0] + (buffer[1] * 0x100)) - 1) / 32) - 1;

                string[] FieldName = new string[FieldCount]; // Массив названий полей
                string[] FieldType = new string[FieldCount]; // Массив типов полей

                byte[] FieldSize = new byte[FieldCount]; // Массив размеров полей
                byte[] FieldDigs = new byte[FieldCount]; // Массив размеров дробной части

                buffer = new byte[32 * FieldCount]; // Описание полей: 32 байтa * кол-во, начиная с 33-го
                fs.Position = 32;
                fs.Read(buffer, 0, buffer.Length);
                int FieldsLength = 0;

                for (int i = 0; i < FieldCount; i++)
                {
                    // Заголовки
                    FieldName[i] = Encoding.Default.GetString(buffer, i * 32, 10).TrimEnd(new char[] { (char)0x00 });
                    FieldType[i] = "" + (char)buffer[i * 32 + 11];
                    FieldSize[i] = buffer[i * 32 + 16];
                    FieldDigs[i] = buffer[i * 32 + 17];
                    FieldsLength = FieldsLength + FieldSize[i];

                    // Создаю колонки
                    switch (FieldType[i])
                    {
                        case "C":
                            table.Columns.Add(FieldName[i], Type.GetType("System.String"));
                            break;
                        case "L":
                            table.Columns.Add(FieldName[i], Type.GetType("System.Boolean"));
                            break;
                        case "D":
                            table.Columns.Add(FieldName[i], Type.GetType("System.DateTime"));
                            break;
                        case "N":
                            if (FieldDigs[i] == 0)
                                table.Columns.Add(FieldName[i], Type.GetType("System.Int32"));
                            else
                                table.Columns.Add(FieldName[i], Type.GetType("System.Decimal"));
                            break;
                        case "F":
                            table.Columns.Add(FieldName[i], Type.GetType("System.Double"));
                            break;
                        default:
                            //GTable.Columns.Add(FieldName[i], Type.GetType("System.String"));//////////////////?!!!
                            break;
                    }
                }
                fs.ReadByte(); // Пропускаю разделитель схемы и данных

                DateTimeFormatInfo dfi = new CultureInfo("en-US", false).DateTimeFormat;
                NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

                buffer = new byte[FieldsLength];
                table.BeginLoadData();

                for (int j = 0; j < RowsCount; j++)
                {
                    fs.ReadByte(); // Пропускаю стартовый байт элемента данных
                    fs.Read(buffer, 0, buffer.Length);
                    DataRow R = table.NewRow();
                    int Index = 0;

                    for (int i = 0; i < FieldCount; i++)
                    {
                        //string l = Encoding.GetEncoding(Encoding.UTF8.HeaderName).GetString(buffer, Index, FieldSize[i]).TrimEnd(new char[] { (char)0x00 }).TrimEnd(new char[] { (char)0x20 });
                        string l = Encoding.GetEncoding(866).GetString(buffer, Index, FieldSize[i]).TrimEnd(new char[] { (char)0x00 }).TrimEnd(new char[] { (char)0x20 });
                        Index = Index + FieldSize[i];

                        if (l != "")
                            switch (FieldType[i])
                            {
                                case "L":
                                    R[i] = l == "T" ? true : false;
                                    break;
                                case "D":
                                    R[i] = DateTime.ParseExact(l, "yyyyMMdd", dfi);
                                    break;
                                case "N":
                                    if (FieldDigs[i] == 0)
                                        R[i] = int.Parse(l, nfi);
                                    else
                                        R[i] = decimal.Parse(l, nfi);
                                    break;
                                case "F":
                                    R[i] = double.Parse(l, nfi);
                                    break;
                                default:
                                    R[i] = l;
                                    break;
                            }
                        else
                            R[i] = DBNull.Value;
                    }
                    table.Rows.Add(R);
                    //Application.DoEvents();
                }
                table.EndLoadData();
                fs.Close();
            }
        }

        private static string YMD2DMY(string s) //yyyymmdd -> dd.mm.yyyy
        {
            if (s.Length == 8)
                return string.Format("{0}.{1}.{2}", s.Substring(6, 2), s.Substring(4, 2), s.Substring(0, 4));
            else
                return "";
        }

        private static string DMY2YMD(string s) //dd.mm.yyyy -> yyyymmdd
        {
            if (s.Length == 10)
                return string.Format("{0}{1}{2}", s.Substring(6, 4), s.Substring(3, 2), s.Substring(0, 2));
            else
                return "";
        }

        private static string StripSpaces(string s)
        {
            while (s.Contains("  "))
                s = s.Replace("  ", " ");
            return s;
        }

        private static void Problem(PayDoc doc, string part, ref string field, string msg)
        {
            string sfield = (field.Length > CutLength) ? field.Substring(0, CutLength) + "..." : field;
            string prompt = string.Format("Уточните N{0} на {1}", doc.DocNo, doc.Sum);
            string ask = string.Format("{0} {2} ({1})", part, sfield, msg);
            string log = string.Format("{0} \"{1}\" - {2}", part, sfield, msg);

            if (InputBox.Query(prompt, ask, ref field))
                Console.WriteLine("     Ошибка: {0} - исправлено на: {1}", ask, field);
            else
                //throw new Exception(log);
                File.AppendAllText(LogFile, log, cp866);
        }

        private static void ProbEx(PayDoc doc, string part, ref string field, string regexp, string msg = "")
        {
            if (regexp.Contains("~") && msg.Length == 0)
            {
                string[] parts = regexp.Split(new char[] { '~' });
                regexp = parts[0];
                msg = parts[1];
            }
            Regex regex = new Regex(regexp);
            while (!regex.IsMatch(field))
                Problem(doc, part, ref field, msg);
        }

        private static void VerifyText(PayDoc doc, string part, ref string field, int min = 0, int max = 0)
        {
            if (min > 0)
                while (field.Length < min)
                    Problem(doc, part, ref field, string.Format("короче {0} символов", min));
            if (max > 0)
                while (field.Length > max)
                    Problem(doc, part, ref field, string.Format("длиннее {0} символов", max));
            //while (field.Contains("^"))
            //    Problem(doc, part, ref field, "содержит ^");
            while (field.Contains("..."))
                Problem(doc, part, ref field, "содержит многоточие");
            //while (field.StartsWith("\""))
            //    Problem(doc, part, ref field, "кавычка в начале");
            while (field.StartsWith("-"))
                Problem(doc, part, ref field, "минус в начале");
        }

        private static void VerifyINN(PayDoc doc, string part, ref string field, string LS)
        {
            ProbEx(doc, part, ref field, Properties.Settings.Default.REGEXP_INN);
            //while (!ValidINNKey(field)) //////////////////////////////////
            //    Problem(doc, part, ref field, "неправильный");
            while (LS.StartsWith("40") && field.Equals(OurINN))
                Problem(doc, part, ref field, "это ИНН Банка");
        }

        private static void VerifyLS(PayDoc doc, string part, ref string ls, string bic)
        {
            ProbEx(doc, part, ref ls, Properties.Settings.Default.REGEXP_LS);
        }

        //public static string InputBox(string Prompt, string Title = "", string DefaultResponse = "")
        //{
        //    string reply = Microsoft.VisualBasic.Interaction.InputBox(Prompt, Title, DefaultResponse);
        //    return reply;
        //}
    }
}
